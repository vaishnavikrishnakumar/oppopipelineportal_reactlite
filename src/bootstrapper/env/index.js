export const isProduction = !!process.env.isProduction

export const enableProxy = false
export const APP_PORT = 5000
export const PROXY_PORT = 4000

export const API_BASE_PATH = enableProxy
  ? `http://localhost:${PROXY_PORT}/orchestrationservices/storefront`
  : 'https://api.skavacommerce.com/orchestrationservices/storefront'

export const API_CONFIG = {
  basePath: `${API_BASE_PATH}`,
  catalogBasePath: `${API_BASE_PATH}/catalogs`,
  customerBasePath: `${API_BASE_PATH}/customers`,
  cartBasePath: `${API_BASE_PATH}/carts/USER`,
  commonHeaders: {
    'x-store-id': '11',
    locale: 'en_US',
    'Content-Type': 'application/json',
  },
}

export const END_POINTS = {
  topCategory: {
    url: `${API_CONFIG.catalogBasePath}/categories/top`,
    method: 'GET',
  },
  getCatalog: {
    url: `${API_CONFIG.catalogBasePath}/categories`,
    method: 'GET',
  },
  getProducts: {
    url: `${API_CONFIG.catalogBasePath}/products`,
    method: 'GET',
  },
  search: {
    url: `${API_CONFIG.catalogBasePath}/search`,
    method: 'GET',
  },
  suggestions: {
    url: `${API_CONFIG.catalogBasePath}/suggestions`,
    method: 'GET',
  },
  login: {
    url: `${API_CONFIG.customerBasePath}/login`,
    method: 'POST',
  },
  logout: {
    url: `${API_CONFIG.customerBasePath}/logout`,
    method: 'DELETE',
  },
  getProfile: {
    url: `${API_CONFIG.customerBasePath}`,
    method: 'GET',
  },
  register: {
    url: `${API_CONFIG.customerBasePath}`,
    method: 'POST',
  },
  addAddress: {
    url: `${API_CONFIG.customerBasePath}/addresses`,
    method: 'POST',
  },
  getAddress: {
    url: `${API_CONFIG.customerBasePath}/addresses`,
    method: 'GET',
  },
  deleteAddress: {
    url: `${API_CONFIG.customerBasePath}/addresses`,
    method: 'DELETE',
  },
  updateAddress: {
    url: `${API_CONFIG.customerBasePath}/addresses`,
    method: 'PATCH',
  },
  getPayment: {
    url: `${API_CONFIG.customerBasePath}/payments`,
    method: 'GET',
  },
  addPayment: {
    url: `${API_CONFIG.customerBasePath}/payments`,
    method: 'POST',
  },
  deletePayment: {
    url: `${API_CONFIG.customerBasePath}/payments`,
    method: 'DELETE',
  },
  updatePayment: {
    url: `${API_CONFIG.customerBasePath}/payments`,
    method: 'PATCH',
  },
  viewBag: {
    url: `${API_CONFIG.cartBasePath}`,
    method: 'GET',
  },
  addToBag: {
    url: `${API_CONFIG.cartBasePath}`,
    method: 'POST',
  },
  deleteCartItem: {
    url: `${API_CONFIG.cartBasePath}/items`,
    method: 'DELETE',
  },
  updateBag: {
    url: `${API_CONFIG.cartBasePath}/items`,
    method: 'PATCH',
  },
}
