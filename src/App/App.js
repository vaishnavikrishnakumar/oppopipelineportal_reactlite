import React from 'react'
import ReactDOM from 'react-dom'
import {BrowserRouter as Router} from 'react-router-dom'
import {AppContainer} from 'react-hot-loader'
import {Appprovider} from './Appcontext'
import './styles.scss'

ReactDOM.render(
  <AppContainer>
    <Router>
      <Appprovider />
    </Router>
  </AppContainer>,
  document.getElementById('app-root')
)
