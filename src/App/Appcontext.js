import React, {createContext, useEffect, useState} from 'react'
import Routes from 'src/routes/Routes'
import {getUserdetails} from 'src/Endpoints/User'
import {
  useGetAllOpportunitytype,
  useGetAllPricingmodel,
  useGetAllPartner,
  useGetAllstatus,
  useGetAllmastercode,
  useGetAllwinlossreason,
} from 'src/Endpoints/Opportunity'

export const Appcontext = createContext()

function getCommonopportunity() {
  const [allmcc, setMcc] = useState([])
  const opportunitytypes = useGetAllOpportunitytype() || []
  const pricingmodels = useGetAllPricingmodel() || []
  const partenrunits = useGetAllPartner() || []
  const statuslist = useGetAllstatus() || []
  const masterCustomerCodes = useGetAllmastercode() || []
  const winlossreasons = useGetAllwinlossreason() || []

  useEffect(() => {
    setMcc(masterCustomerCodes)
  }, [masterCustomerCodes])

  return {
    opportunitytypes,
    pricingmodels,
    partenrunits,
    statuslist,
    winlossreasons,
    setMcc,
    allmcc,
  }
}

function Appprovider() {
  const value = getUserdetails()
  const Datas = {
    user: value,
    opportunities: getCommonopportunity(),
  }
  return (
    <Appcontext.Provider value={Datas}>
      <Routes />
    </Appcontext.Provider>
  )
}

export {Appprovider}

export default Appprovider
