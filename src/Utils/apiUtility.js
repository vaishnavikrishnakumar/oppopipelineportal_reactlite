import React, {useState} from 'react'
import {URL_DETAILS} from 'src/Endpoints/Apidetails'
import {useLoader} from 'src/views/components'

function usefetchresponseAPI(props) {
  const {setLoad} = useLoader()
  const [response, setResponse] = useState({})
  const {endpointName, ...remainingprops} = props
  const Fetch = async actionprops => {
    const fetchprops = {
      endpointName: endpointName,
      ...remainingprops,
      ...actionprops,
    }
    setLoad(true)
    const data = await fetchAPI(fetchprops)
    setResponse(data)
    setLoad(false)
    return data
  }

  // useMemo(async () => await Fetch(), [])

  return {
    action: Fetch,
    response,
  }
}

async function fetchAPI(props) {
  const {
    headers = {'Content-Type': 'application/json'},
    queryparams,
    endpointName,
    postdata,
  } = props
  const {method, url} = URL_DETAILS[endpointName]

  let finalURL = url

  if (queryparams) {
    finalURL += '?'
  }

  for (let query in queryparams) {
    finalURL += `${query}=${queryparams[query]}&`
  }
  const fetchprops = {
    headers,
    method,
    body: JSON.stringify(postdata),
  }
  return await fetch(finalURL, fetchprops)
    .then(response => {
      return response.json().then(data => {
        data.status = response.status
        return data
      })
    })
    .catch(e => {
      console.log('API Fail')
      return {}
    })
}

export default usefetchresponseAPI
export {fetchAPI, usefetchresponseAPI}
