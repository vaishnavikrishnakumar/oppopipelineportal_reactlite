import {useContext} from 'react'
import {Appcontext} from 'src/App/Appcontext'

const getMastercode = masterCustomerCodes => {
  if (Array.isArray(masterCustomerCodes)) {
    return masterCustomerCodes.map(code => {
      return {
        label: code.masterCustomerCode,
        value: code.masterCustomerCode,
        fullValue: JSON.stringify(code),
      }
    })
  } else {
    return []
  }
}

const isValidArray = val => {
  return Array.isArray(val)
}

const transformNewopprtunity = data => {
  const {
    licenseSale,
    submitbutton,
    annualSubscription,
    totalContractValueInfy,
    totalContractValueEquinox,
    contractDurationInMonths,
    partnerLedRfp,
    newAccountOpening,
    ...remainingprops
  } = data

  return {
    submitbutton: undefined,
    annualSubscription: parseInt(annualSubscription),
    totalContractValueInfy: parseInt(totalContractValueInfy),
    totalContractValueEquinox: parseInt(totalContractValueEquinox),
    contractDurationInMonths: parseInt(contractDurationInMonths),
    licenseSale: licenseSale ? 'Yes' : 'No',
    partnerLedRfp: partnerLedRfp ? 'Yes' : 'No',
    newAccountOpening: newAccountOpening ? 'Yes' : 'No',
    ...remainingprops,
  }
}

const getFormattedoptions = props => {
  const {
    response,
    labelField = '',
    valueField = '',
    isaddselect = false,
  } = props
  let options = []
  if (Array.isArray(response)) {
    options = response.map(field => {
      if (typeof field === 'string') {
        return {
          label: field,
          value: field,
        }
      } else {
        return {
          label: field[labelField],
          value: field[valueField],
        }
      }
    })
  }

  if (isaddselect) {
    options.splice(0, 0, {
      label: '-Select-',
      value: '',
    })
    return options
  }

  return options
}

const useAdditionaldata = isaddselect => {
  const {opportunities} = useContext(Appcontext)
  const {
    partenrunits,
    opportunitytypes,
    pricingmodels,
    statuslist,
    winlossreasons,
    allmcc,
  } = opportunities

  return {
    partenrunits: getFormattedoptions({
      response: partenrunits,
      valueField: 'partnerUnit',
      labelField: 'partnerUnit',
      isaddselect,
    }),
    opportunitytypes: getFormattedoptions({
      response: opportunitytypes,
      isaddselect,
    }),
    pricingmodels: getFormattedoptions({
      response: pricingmodels,
      isaddselect,
    }),
    statuslist: getFormattedoptions({
      response: statuslist,
      isaddselect,
    }),
    masterCustomerCodes: getMastercode(allmcc),
    winlossreasons: getFormattedoptions({
      response: winlossreasons,
      isaddselect,
    }),
  }
}

export default getMastercode

export {
  getMastercode,
  transformNewopprtunity,
  getFormattedoptions,
  useAdditionaldata,
}
