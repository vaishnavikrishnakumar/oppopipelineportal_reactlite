import React from 'react'
import {Table, Form, Button} from 'react-bootstrap'
import './style.scss'

function GetTableContent(props) {
  const {titles, opp, handler} = props
  return (
    <>
      <tr>
        <td className="text-center">
          <input
            type="checkbox"
            className="editcheckbox"
            disabled={handler.isEnableedit(opp)}
            onClick={() => {
              handler.AddEditlist(opp)
            }}
          />
        </td>
        {titles.map(field => {
          const {value, type, options = [], className} = field
          if (type === 'textarea') {
            return (
              <td key={field.value} className="py-0">
                <textarea
                  className={`${value} ${className}`}
                  defaultValue={opp[value]}
                  type={type}
                  disabled={handler.isDisable(value, opp['ciaraId'])}
                  onChange={e =>
                    handler.handleOnchange(e, value, opp['ciaraId'])
                  }
                />
              </td>
            )
          }
          return (
            <td key={field.value}>
              {type === 'select' && (
                <select
                  selected={opp[value]}
                  defaultValue={opp[value]}
                  disabled={handler.isDisable(value, opp['ciaraId'])}
                  className={`px-2 py-1 ${className} ${value}`}
                  onChange={e =>
                    handler.handleOnchange(e, value, opp['ciaraId'])
                  }>
                  {options.length > 0 &&
                    options.map(opt => {
                      return (
                        <option {...opt} selected={opp[value] === opt.label}>
                          {opt.label}
                        </option>
                      )
                    })}
                </select>
              )}
              {type !== 'select' && (
                <input
                  defaultValue={opp[value] ? opp[value] : '-'}
                  className={`${value} ${className} ${
                    opp[value] ? '' : 'text-center'
                  }`}
                  type={type}
                  disabled={handler.isDisable(value, opp['ciaraId'])}
                  onChange={e =>
                    handler.handleOnchange(e, value, opp['ciaraId'])
                  }
                />
              )}
            </td>
          )
        })}
      </tr>
    </>
  )
}

const TableBase = props => {
  const {titles = [], displaycontent = [], handler} = props
  return (
    <Table bordered striped hover responsive size="md" className="tableform">
      <thead>
        <tr>
          <th>Edit</th>
          {titles.map((title, index) => {
            return <th key={index}>{title.label}</th>
          })}
        </tr>
      </thead>
      <tbody>
        {displaycontent.map((opp, index) => {
          return <GetTableContent titles={titles} opp={opp} handler={handler} />
        })}
      </tbody>
    </Table>
  )
}

function FormTable(props) {
  const {titles, displaycontent, isFormtable = false, handler, id = ''} = props
  if (isFormtable) {
    return (
      <Form
        id={id}
        className={`${id}form`}
        onSubmit={data => {
          data.preventDefault()
          data.target.reset()
        }}>
        <TableBase
          titles={titles}
          displaycontent={displaycontent}
          handler={handler}
        />
      </Form>
    )
  }
  return <TableBase {...props} />
}

export {FormTable}

export default FormTable
