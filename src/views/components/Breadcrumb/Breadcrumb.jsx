import React from 'react'
import {Breadcrumb} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import './style.scss'

const getDefaultList = () => {
  return {
    label: 'Home',
    value: '/',
  }
}

const getBreadcrumbview = lists => {
  return lists.map((list, index) => {
    const {value = '', label} = list
    if (value != '') {
      return (
        <Link
          data-testid="qa-breadcrumb"
          className="breadcrumblink  breadcrumb-item-list"
          key={index}
          to={value}>
          {label}
        </Link>
      )
    } else {
      return (
        <span
          data-testid="qa-breadcrumb"
          className="breadcrumb-item-list"
          key={index}>
          {label}
        </span>
      )
    }
  })
}

function BreadcrumbList(props) {
  const {listData = []} = props
  const lists = [getDefaultList()]
  lists.push(...listData)
  return (
    <Breadcrumb
      className="rounded-0"
      label="breadcrumb"
      listProps={{
        className:
          'bg-white m-0 h-auto px-3 align-items-center py-1 text-truncate flex-nowrap',
      }}>
      {getBreadcrumbview(lists)}
    </Breadcrumb>
  )
}

export {BreadcrumbList}

export default BreadcrumbList
