import React, {useEffect} from 'react'
import {useFormik} from 'formik'
import {Form, Col, Row, FloatingLabel} from 'react-bootstrap'

const RenderOptions = (Options, index) => {
  const {value, label, ...reaminingprops} = Options
  return (
    <option
      value={Options.value}
      {...reaminingprops}
      key={`${Options.value}_${index}`}>
      {Options.label}
    </option>
  )
}

const checkInvalid = props => {
  const {errors, touched, name} = props
  if (errors && errors[name]) {
    if (!Array.isArray(touched[name]) && touched[name]) {
      return true
    } else if (Array.isArray(touched[name]) && touched[name].length === 0) {
      return true
    } else {
      return false
    }
  }
  return false
}

const RenderFormfield = props => {
  const {
    handleChange,
    type,
    options,
    onOptionChange,
    labelValue,
    name,
    autoComplete = 'on',
    values,
  } = props
  const updatedlabel = labelValue ? labelValue : name
  if (type === 'select') {
    const handleSelection = event => {
      if (typeof onOptionChange === 'function') {
        onOptionChange(event)
      }
      handleChange(event)
    }
    return (
      <Form.Select
        as={type}
        onChange={handleSelection}
        placeholder={updatedlabel}
        {...props}>
        {options.map(RenderOptions)}
      </Form.Select>
    )
  }
  if (type === 'checkbox' || type === 'radio') {
    const handlevalueChange = event => {
      if (typeof props.onOptionChange === 'function') {
        props.onOptionChange(event)
      }
      values[name] = event.target.checked
      props.handleChange(event)
    }
    return (
      <Form.Check
        id={`custom-${props.name}`}
        {...props}
        label={labelValue}
        onChange={handlevalueChange}
      />
    )
  }

  if (type === 'date') {
    const handlevalueChange = event => {
      props.handleChange(event)
    }
    return <Form.Control type="date" onChange={handlevalueChange} {...props} />
  }
  return (
    <Form.Control
      onChange={handleChange}
      autoComplete={autoComplete}
      placeholder={updatedlabel}
      {...props}
    />
  )
}

const RenderFormelement = props => {
  const {
    type,
    handleBlur,
    touched,
    Buttoncomponent,
    name,
    errors,
    isFloatingLabel,
    labelValue,
    isHideLabel = false,
    autoComplete,
  } = props

  const updatedlabel = labelValue ? labelValue : name
  const onBlur = event => {
    handleBlur && handleBlur(event)
  }

  const isValid =
    type !== 'select' &&
    type !== 'checkbox' &&
    touched[name] !== undefined &&
    !errors[name]
  const isInvalid = checkInvalid(props)
  const controlprops = {
    onBlur,
    isValid,
    isInvalid,
    errors,
    autoComplete,
  }
  if (type === 'button') {
    return <Buttoncomponent {...controlprops} {...props} />
  }

  if (isFloatingLabel && type !== 'checkbox' && type !== 'radio') {
    return (
      <FloatingLabel
        controlId="floatingInput"
        label={updatedlabel}
        className="px-2 my-2">
        <RenderFormfield {...controlprops} {...props} />
        <Form.Control.Feedback type="invalid">
          {errors[name]}
        </Form.Control.Feedback>
      </FloatingLabel>
    )
  }
  return (
    <>
      {!isHideLabel && <Form.Label> {updatedlabel} </Form.Label>}
      <RenderFormfield {...controlprops} {...props} />
      <Form.Control.Feedback type="invalid">
        {errors[name]}
      </Form.Control.Feedback>
    </>
  )
}

function RenderBaseofField(props) {
  const {formValues} = props
  return formValues.map((fields, index) => {
    if (Array.isArray(fields)) {
      return (
        <Row className="mw-100 px-3" key={`${index}`}>
          {fields.map(field => {
            const {name, columnType} = field
            return (
              <Col {...columnType} key={`${name}_${index}`}>
                <RenderFormelement {...field} {...props} />
              </Col>
            )
          })}
        </Row>
      )
    } else {
      const {name, columnType} = fields
      return (
        <Row className="mw-100 px-3" key={`${index}`}>
          <Col {...columnType} key={`${name}_${index}`}>
            <RenderFormelement {...fields} {...props} />
          </Col>
        </Row>
      )
    }
  })
}

const getIntialvalues = formInput => {
  let initalvalues = {}
  const flattedformInput = formInput.flat()
  flattedformInput.forEach(field => {
    const {name, defaultValue = ''} = field
    initalvalues[name] = defaultValue ? defaultValue : ''
  })
  return initalvalues
}

function CommonForm(props) {
  const {
    formInput = [],
    Schema,
    formClassName,
    isFloatingLabel = true,
    ...reaminingprops
  } = props
  const initalvalues = getIntialvalues(formInput)

  const handleSubmit = (data, {resetForm}) => {
    const {onSubmit} = props
    resetForm()
    if (onSubmit) {
      onSubmit(data)
    }
  }

  const formik = useFormik({
    initialValues: initalvalues,
    onSubmit: handleSubmit,
    validationSchema: Schema,
    isInitialValid: false,
    ...reaminingprops,
  })
  const propstopass = {
    ...formik,
    formValues: formInput,
    isFloatingLabel,
    ...reaminingprops,
  }
  return (
    <Form
      className={`${isFloatingLabel ? 'floatingForm' : ''} ${formClassName}`}
      onSubmit={e => {
        e.preventDefault()
        handleSubmit(formik.values, {...formik})
        e.target.reset()
      }}>
      <RenderBaseofField {...propstopass} />
    </Form>
  )
}

export {CommonForm}

export default CommonForm
