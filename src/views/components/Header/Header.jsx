import React, {useContext} from 'react'
import {Col, Row} from 'react-bootstrap'
import {Link, withRouter} from 'react-router-dom'
import {Categories} from './Categories'
import {pageNames} from 'src/routes/pathParams'
import {Appcontext} from 'src/App/Appcontext'
import './style.scss'

function PortalLogo() {
  return (
    <Col
      lg={2}
      md={3}
      sm={8}
      className="logo-container mw-100 d-flex justify-content-center">
      <Link to={pageNames.home}>
        <img
          src="https://raderain.sirv.com/Presales/Pursuits%20Portal/Logo/UILogo.png"
          className="mw-100 logo-image"
        />
      </Link>
    </Col>
  )
}

function UserList() {
  const {user} = useContext(Appcontext)
  return (
    <Col
      lg={4}
      md={3}
      sm={4}
      className="logo-container mw-100 pt-2 d-none d-lg-block d-lg-flex align-items-center justify-content-end">
      <div className="py-2 px-3 bg-primary text-white">Welcome User</div>
    </Col>
  )
}

function Header() {
  return (
    <section className="header-section py-3 mb-2">
      <Row className="mw-100">
        <PortalLogo />
        <Categories />
        <UserList />
      </Row>
    </section>
  )
}

export {Header}

export default withRouter(Header)
