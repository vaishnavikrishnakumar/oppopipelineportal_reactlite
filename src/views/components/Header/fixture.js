const CategoryList = userRole => {
  const Admincategories = [
    {
      name: 'Import / Export',
      subcategories: [
        {
          label: 'Import Data',
          value: 'import',
          link: 'import',
        },
        {
          label: 'Export Data',
          value: 'export',
        },
      ],
    },
    {
      name: 'Admin',
      subcategories: [
        {
          label: 'Customer code Bulk Import',
          value: 'import',
          link: 'import',
        },
        {
          label: 'Customer code Manual Entry',
          value: 'manualentry',
          link: 'manualentry',
        },
      ],
    },
  ]

  const usercategories = [
    {
      name: 'Export Data',
      subcategories: [],
    },
  ]
  return userRole === 'Admin' ? Admincategories : usercategories
}
export {CategoryList}
