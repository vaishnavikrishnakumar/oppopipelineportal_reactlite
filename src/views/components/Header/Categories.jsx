import React, {useState, useContext} from 'react'
import {Link} from 'react-router-dom'
import {Col, Dropdown, Container} from 'react-bootstrap'
import {pageNames} from 'src/routes/pathParams'
import {CategoryList} from './fixture'
import {usefetchresponseAPI} from 'src/Utils'
import './style.scss'

function GetOthercategory(props) {
  const {list = {}} = props
  const {name = '', subcategories = []} = list
  const [isCategoryVisible, setisCategoryVisible] = useState(false)
  const exportaction = usefetchresponseAPI({
    endpointName: 'exportopp',
    headers: {
      accept:
        'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    },
  })
  const handleToggle = async label => {
    if (label === 'Export Data') {
      await exportaction.action()
    }
    setisCategoryVisible(!isCategoryVisible)
  }
  const renderOptions = oprionslist => {
    return oprionslist.map((option, index) => {
      const {label, value, link = ''} = option
      const Borderclass =
        index !== oprionslist.length - 1 ? 'border-bottom' : ''
      if (link != '') {
        return (
          <Dropdown.Item className={`px-1 ${Borderclass}`} key={index}>
            <Link className="headerLink" to={pageNames[link]}>
              <Col
                className="text-dark categorypointer p-0"
                value={value}
                onClick={handleToggle}
                key={index}>
                {label}
              </Col>
            </Link>
          </Dropdown.Item>
        )
      }
      return (
        <Dropdown.Item className={`px-1 ${Borderclass}`} key={index}>
          <Col
            className="text-dark categorypointer p-0"
            value={value}
            onClick={() => handleToggle(label)}
            key={index}>
            {label}
          </Col>
        </Dropdown.Item>
      )
    })
  }

  return (
    <Col lg={3} md={6} className="d-flex justify-content-center px-0 pt-2">
      <Dropdown
        drop={isCategoryVisible ? 'up' : 'down'}
        className={`header-wrapper pt-1 ${isCategoryVisible ? 'visible' : ''}`}
        onToggle={handleToggle}
        show={isCategoryVisible}>
        <Dropdown.Toggle
          variant=""
          id={`${name}`}
          className="menu-btn pt-1 d-none d-lg-block  border-0"
          data-testid="qa-category">
          {name}
        </Dropdown.Toggle>
        {subcategories.length > 0 && (
          <Dropdown.Menu>
            <Container className="position-relative px-0 overflow-hidden category-container">
              {renderOptions(subcategories)}
            </Container>
          </Dropdown.Menu>
        )}
      </Dropdown>
    </Col>
  )
}

function Categories() {
  const [userRole, setuserRole] = useState('Admin')
  const categoriesList = CategoryList(userRole)
  return (
    <Col lg={6} md={4} sm={4} className="d-none d-lg-block d-lg-flex">
      <Col
        lg={2}
        md={6}
        className="d-flex justify-content-center align-items-center px-0 pt-2">
        <Link className="headerLink" to={pageNames.newentry}>
          New Entry
        </Link>
      </Col>
      {userRole !== 'Admin' && (
        <Col
          lg={2}
          md={6}
          className="d-flex justify-content-center align-items-center px-0 pt-2">
          <Link className="headerLink" to={pageNames.newentry}>
            Export Data
          </Link>
        </Col>
      )}
      {userRole === 'Admin' &&
        categoriesList.map((category, index) => {
          return <GetOthercategory list={category} key={index} />
        })}
    </Col>
  )
}

export {Categories}

export default Categories
