import React, {useContext, useState} from 'react'
import {Pagination as Paginationbar} from 'react-bootstrap'
import {Oppcontext} from 'src/views/pages/OpportunityLanding'

function Pagination(props) {
  const {pageHandler} = props
  const {totalcount} = useContext(Oppcontext)
  const [activeidx, setActive] = useState(0)

  let Pagescount = totalcount / 10
  let decimal = Pagescount - Math.floor(Pagescount)
  console.log(decimal)
  Pagescount = Number.isInteger(Pagescount)
    ? Pagescount
    : decimal < 0.5
    ? Math.round(Pagescount) + 1
    : Math.round(Pagescount)
  let pagesArray = Array(Pagescount)
    .fill(0)
    .map((el, index) => index + 1)

  const getPageitem = (pageno, index) => {
    return (
      <Paginationbar.Item
        className={`border-0 pagecount ${
          activeidx === index ? 'text-primary active' : 'text-dark'
        }`}
        key={index}
        onClick={() => {
          setActive(index)
          typeof pageHandler === 'function' && pageHandler(pageno)
        }}>
        {pageno}
      </Paginationbar.Item>
    )
  }
  const handleicon = action => {
    if (action === 'prev') {
      let pageid = activeidx === 0 ? Pagescount - 1 : activeidx - 1
      setActive(Math.abs(pageid))
      pageHandler(Math.abs(pageid) + 1)
    } else {
      let pageid = activeidx === Pagescount - 1 ? 0 : activeidx + 1
      setActive(Math.abs(pageid))
      pageHandler(Math.abs(pageid) + 1)
    }
  }

  if (Pagescount === 1) {
    return ''
  }
  return (
    <Paginationbar className="border-0">
      <Paginationbar.Prev
        onClick={() => typeof pageHandler === 'function' && handleicon('prev')}
      />
      {pagesArray.map((pageno, index) => getPageitem(pageno, index))}
      <Paginationbar.Next
        onClick={() => typeof pageHandler === 'function' && handleicon('next')}
      />
    </Paginationbar>
  )
}

export {Pagination}

export default Pagination
