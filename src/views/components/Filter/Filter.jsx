import React, {useState, useContext, useEffect} from 'react'
import {
  Form,
  Col,
  Dropdown,
  Container,
  InputGroup,
  Button,
} from 'react-bootstrap'
import {filtercategories} from './fixture'
import {BiSearch as SearchIcon} from 'react-icons/bi'
import {HiOutlineRefresh as RefreshIcon} from 'react-icons/hi'
import {useAdditionaldata, usefetchresponseAPI} from 'src/Utils'
import {Oppcontext} from 'src/views/pages/OpportunityLanding'
import './style.scss'

function ListForm(props) {
  const {filterHandler, list, groudid} = props
  return (
    <Col className={`px-0 py-1  d-flex ${list.value}`}>
      <div>
        <input
          type="checkbox"
          className="filter-list"
          onChange={e => filterHandler(list, e, groudid)}
        />
      </div>
      <div className="px-1">{list.label}</div>
    </Col>
  )
}

function Searchblock(props) {
  const [searchValue, setSearch] = useState('')
  const {label, searchHandler} = props
  return (
    <>
      <Form
        className="d-flex align-items-center"
        onSubmit={e => {
          e.preventDefault()
          searchHandler(searchValue)
        }}>
        <InputGroup className="mb-1">
          <Form.Control
            name="searchID"
            className="d-flex px-0 align-items-end "
            onChange={e => setSearch(e.target.value)}
            placeholder={label}
          />
          <Button
            variant=""
            className="border-bottom filter-category rounded-0"
            type="submit"
            id="button-addon1">
            <SearchIcon />
          </Button>
        </InputGroup>
      </Form>
    </>
  )
}

function Filter() {
  const {setOpprtunities} = useContext(Oppcontext)
  const opportunities = usefetchresponseAPI({
    endpointName: 'getAllopp',
  })

  useEffect(() => {
    if (opportunities.response.status == 200) {
      setOpprtunities(opportunities.response.opportunities)
    }
  }, [opportunities.response])

  const constructQuery = (groudid, value) => {}

  const filterHandler = async (value, event, groudid) => {
    if (value === 'reset') {
      setOpprtunities([])
    } else {
      console.log(value, groudid)
      constructQuery()
      if (event.target.checked) {
        await opportunities.action()
      }
    }
    console.log('API not avaiable')
  }

  const searchHandler = value => {
    console.log(value)
    console.log('API not avaiable')
  }

  const {partenrunits, statuslist, masterCustomerCodes} = useAdditionaldata(
    false
  )

  const additionalData = {
    partenrunits,
    statuslist,
    masterCustomerCodes,
  }
  const filterCategories = filtercategories(additionalData)

  return (
    <>
      <Container className="filter-wrapper  mw-100 p-0 ">
        {filterCategories.map(data => {
          const filtervalue = data.options || [
            {
              label: 'Filter 1',
              value: '',
            },
            {
              label: 'Filter 2',
              value: '',
            },
            {
              label: 'Filter 3',
              value: '',
            },
          ]
          if (data.type === 'input') {
            return <Searchblock searchHandler={searchHandler} {...data} />
          }
          return (
            <Dropdown className="d-flex align-items-center" drop="down">
              <Dropdown.Toggle
                variant=""
                className={`w-100 filter-category border-bottom rounded-0 text-left filterText pb-1 px-0 py-2 d-flex justify-content-between ${
                  data.label === 'Master Customer Code' ||
                  data.label === 'Industry Segment'
                    ? 'mcctoggle'
                    : data.label
                }`}>
                {data.label}
              </Dropdown.Toggle>
              <Dropdown.Menu className="w-100 p-2">
                {filtervalue.map(list => {
                  const propstopass = {
                    filterHandler,
                    list,
                    groudid: data.groudid,
                  }
                  return ListForm(propstopass)
                })}
              </Dropdown.Menu>
            </Dropdown>
          )
        })}
      </Container>
      <Col className="d-flex align-items-center pt-2 justify-content-end">
        <RefreshIcon
          size={25}
          onClick={() => filterHandler('reset')}
          className={'filter-list'}
        />
      </Col>
    </>
  )
}

export {Filter}

export default Filter
