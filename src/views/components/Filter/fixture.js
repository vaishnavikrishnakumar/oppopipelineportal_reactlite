export const filtercategories = additionalData => {
  return [
    {
      label: 'ID',
      type: 'input',
    },
    {
      label: 'Partner Unit',
      type: 'list',
      groudid: 'partnerunit',
      options: additionalData.partenrunits,
    },
    {
      label: 'Region',
      type: 'list',
      groudid: 'region',
    },
    {
      label: 'Industry Segment',
      type: 'list',
      groudid: 'Industrysegment',
    },
    {
      label: 'Master Customer Code',
      type: 'list',
      groudid: 'masterCustomercode',
      options: additionalData.masterCustomerCodes,
    },
    {
      label: 'Customer Name',
      type: 'list',
      groudid: 'clientName',
    },
    {
      label: 'Deal Status',
      type: 'multilist',
      groudid: 'status',
      options: additionalData.statuslist,
    },
    {
      label: 'NAO',
      type: 'list',
      groudid: 'nao',
    },
    {
      label: 'Proposal Anchor',
      type: 'list',
      groudid: 'proposalanchor',
    },
  ]
}
