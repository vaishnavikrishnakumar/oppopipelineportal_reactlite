import React from 'react'
import * as ReactDOM from 'react-dom'
import './style.scss'

let toastOptions = {
  wrapperId: 'toast-wrapper',
  timeout: 2000,
  autoClose: true,
  success: 'green',
  warning: 'red',
  default: 'black',
  onCloseClick: () => {},
}

function CustomToast(props) {
  const {
    message,
    type = 'default',
    className = 'rounded',
    onCloseToast = () => {},
    // children,
    testId = '',
    toastWidth = '',
  } = props

  return (
    <div
      className={`${className}`}
      style={{background: toastOptions[type], width: toastWidth}}>
      <div data-testid={testId} className="toast-msg">
        {message}
      </div>
      <div
        className="toast-close"
        role="button"
        tabIndex="0"
        onClick={onCloseToast}>
        Ok
      </div>
    </div>
  )
}

function showToast(props) {
  const {
    message = '',
    type = '',
    testId = '',
    className = '',
    triggerCloseOnTimeout = false,
    ...rest
  } = props
  const {wrapperId, autoClose, timeout, onCloseClick} = toastOptions

  const closeToast = () => {
    hideToast(targetDom)
    if (onCloseClick) {
      onCloseClick()
    }
  }

  const targetDom = document.getElementById(wrapperId)

  if (targetDom) {
    ReactDOM.render(
      <CustomToast
        className={`${className} toastContainer`}
        {...{type, message, testId}}
        onCloseToast={closeToast}
        {...rest}
      />,
      targetDom
    )
  }

  if (autoClose) {
    setTimeout(() => {
      const targetDom = document.getElementById(wrapperId)

      triggerCloseOnTimeout ? closeToast() : hideToast(targetDom)
    }, timeout)
  }
}

const hideToast = targetDom => {
  if (typeof document != 'undefined') {
    if (targetDom) {
      ReactDOM.unmountComponentAtNode(targetDom)
    }
  }
}

const updateOption = options => {
  toastOptions = {...toastOptions, ...options}
}

function Toast(props) {
  const {options, position} = props

  updateOption(options)
  const {wrapperId} = toastOptions
  return wrapperId !== undefined ? (
    <div id={wrapperId} className={`${position} toast-wrapper`} />
  ) : null
}

export {CustomToast, showToast, Toast}
export default CustomToast
