import React from 'react'
import './styles.scss'
import {useLoader} from './loaderstate'

function AppLoader(props) {
  const {show} = useLoader()
  console.log(show)
  if (!show) {
    return ''
  }
  return <div className="appLoader" />
}

export default AppLoader
export {AppLoader, useLoader}
