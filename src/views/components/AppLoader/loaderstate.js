import React, {useState} from 'react'

function useLoader() {
  const [show, setShow] = useState(false)

  const setLoad = value => {
    setShow(value)
  }

  return {
    show,
    setLoad,
  }
}

export {useLoader}

export default useLoader
