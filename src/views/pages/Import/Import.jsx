import React, { useEffect, useState } from 'react'
import {Button, Table, Container, Image} from 'react-bootstrap'
import {BreadcrumbList} from 'src/views/components'
import { Download } from 'react-bootstrap-icons';

function Import(props) {
  const list = [
    {
      label: 'Import',
    },
  ]
  
  const [dataList,setDataList]=useState([])
  const [isLoaded,setIsLoaded]=useState(false)
  const [selectedFile, setSelectedFile] = useState();
	const [isSelected, setIsSelected] = useState(false);
  const [isUploaded,setIsUploaded] = useState(false)

	const changeHandler = (event) => {
    setSelectedFile(event.target.files[0]);
		setIsSelected(true);
	};
  
	const handleSubmission = () => {
    const formData = new FormData();
    console.log("submitted")
		formData.append('file', selectedFile);
    formData.append('userName',"VP");
    console.log(formData);

		fetch(
      'http://localhost:8086/opportunities/upload',
			{
        method: 'POST',
				body: formData,
			}
      )
      .then((response) => response.json())
      .then((result) => {
        console.log('Success:', result);
        setIsUploaded(true)
			})
			.catch((error) => {
        console.error('Error:', error);
			})

      }

      const loadData = async ()=> {
        const response = await fetch(
          'http://localhost:8086/getAllImportedFileInfo',
          {
            method: 'GET',
          }
          );
        const importData1 =await response.json();
        setDataList(importData1);
        setIsLoaded(true);
      }

      useEffect(()=>{
        loadData();
      }
      ,[]      
      )
      
      const importData = dataList
      
      console.log(importData)

      function handelStatus(data) {
        data=data.toLowerCase()
        if(data!=="success"){
          return <td className='text-danger'>{data}</td>
        }else{
      return <td className='text-primary'>{data}</td>
    }
    
  }
  return (
    <Container className="oppo-wrapper mw-100 text-center">
      <BreadcrumbList listData={list} />
      <Container className='pb-2'>
        <div>
          <input id='contained-button-file' type="file" name="file" onChange={changeHandler}
          style={{display:'none'}}
          />
          <label htmlFor="contained-button-file">
            <Image src="https://raderain.sirv.com/Presales/Pursuits%20Portal/creatives/importimg.png" width={50} height={50} alt="No Image" />
            <span id='contained-button-file' className='mr-2'>Import File</span>
          </label>
          <div>
            <Button className="primary text-center m-3" onClick={handleSubmission} color="primary" >
              Upload
            </Button>
          </div>
            {isSelected ? (
            <div>
              <p>Filename: {selectedFile.name}</p>
            </div>
          ) : (
            <p>Select a file to show details</p>
          )}
          {(isUploaded)?(
            <div>
              <h4>File Uploaded</h4>
            </div>
          ):""}
        </div>
      </Container>
      <Table bordered striped hover responsive size="md">
        <thead className='text-light bg-dark'>
          <tr>
            <th>Imported File</th>
            <th>Date</th>
            <th>User Name</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {importData.map(data => (
            <tr key={data.date}>
              <td className='text-primary'>{data.importedFile}</td>
              <td>{data.date}</td>
              <td>{data.userName}</td>
              {handelStatus(data.status)}
              <td><Download/></td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Container>
  )
}

export {Import}

export default Import
