import * as yup from 'yup'

export const validationschema = () => {
  const schema = {
    partnerUnit: yup.string().required('Please Select Partner Unit'),
    masterCustomerCode: yup.string().required('Please Select Master code'),
    customerName: yup.string(),
    opportunityType: yup.string().required('Please Select Opportunity type'),
    opportunityName: yup.string().required('Please Enter Opportunity Name'),
    opportunityDescription: yup.string(),
    totalContractValueInfy: yup
      .number()
      .typeError('Number only Allowed')
      .min(0, 'Minimum Allowed Number is 0')
      .max(1000, 'Maximum Allowed Number is 1000'),
    totalContractValueEquinox: yup
      .number()
      .typeError('Number only Allowed')
      .min(0, 'Minimum Allowed Number is 0')
      .max(1000, 'Maximum Allowed Number is 1000'),
    annualSubscription: yup
      .number()
      .typeError('Number only Allowed')
      .min(0, 'Minimum Allowed Number is 0')
      .max(1000, 'Maximum Allowed Number is 1000'),
    monthlyHostingValue: yup
      .number()
      .typeError('Number only Allowed')
      .min(0, 'Minimum Allowed Number is 0')
      .max(12, 'Maximum Allowed Number is 12'),
    yearlySupport: yup
      .number()
      .typeError('Number only Allowed')
      .min(0, 'Minimum Allowed Number is 0')
      .max(1000, 'Maximum Allowed Number is 1000'),
    services: yup
      .number()
      .typeError('Number only Allowed')
      .min(0, 'Minimum Allowed Number is 0')
      .max(1000, 'Maximum Allowed Number is 1000'),
    contractDurationInMonths: yup
      .number()
      .typeError('Number only Allowed')
      .min(0, 'Minimum Allowed Number is 0')
      .max(12, 'Maximum Allowed Number is 12'),
    remarks: yup.string(),
    pricingModel: yup.string().required('Please Select Pricing Model'),
    proposalAnchor: yup
      .string()
      .max(100, 'Maximum Allowed Charcters is 100')
      .matches(/^[aA-zZ\s]+$/, 'Only alphabets are allowed')
      .required('Please Enter Proposal Anchor'),
    deliveryAnchor: yup
      .string()
      .max(100, 'Maximum Allowed Charcters is 100')
      .matches(/^[aA-zZ\s]+$/, 'Only alphabets are allowed'),
    verticalSales: yup
      .string()
      .max(100, 'Maximum Allowed Charcters is 100')
      .matches(/^[aA-zZ\s]+$/, 'Only alphabets are allowed'),
    practiceSales: yup
      .string()
      .max(100, 'Maximum Allowed Charcters is 100')
      .matches(/^[aA-zZ\s]+$/, 'Only alphabets are allowed'),
    competitor: yup
      .string()
      .max(100, 'Maximum Allowed Charcters is 100')
      .matches(/^[aA-zZ\s]+$/, 'Only alphabets are allowed'),
    submissionDate: yup
      .date()
      .nullable()
      .transform(value => {
        return value instanceof Date && !isNaN(value) ? value : null
      })
      .test(
        'Date Validation',
        'Please select Date between 01/01/2021 to 31/12/2030',
        val => {
          const selectedDate = new Date(val).getFullYear()
          return 2021 <= selectedDate && 2030 >= selectedDate
        }
      ),
  }

  return yup.object(schema)
}
