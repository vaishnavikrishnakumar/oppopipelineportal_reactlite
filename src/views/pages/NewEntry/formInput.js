import {useAdditionaldata} from 'src/Utils'

export const newentryForminput = ({handleMCCselection, customerNamevalue}) => {
  const {
    opportunitytypes,
    partenrunits,
    pricingmodels,
    masterCustomerCodes,
  } = useAdditionaldata(true)
  return [
    [
      {
        name: 'partnerUnit',
        type: 'select',
        labelValue: '*Partner Unit',
        options: partenrunits,
        isHidelabel: true,
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
      {
        name: 'masterCustomerCode',
        type: 'select',
        labelValue: '*Master Customer Code',
        options: [
          {
            label: '-Select-',
            value: '',
          },
          ...masterCustomerCodes,
        ],
        onOptionChange: handleMCCselection,
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
      {
        name: 'customerName',
        type: 'input',
        labelValue: '*Customer Name',
        defaultValue: customerNamevalue !== '' ? customerNamevalue : '',
        autoComplete: 'on',
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
      {
        name: 'opportunityType',
        type: 'select',
        labelValue: '*Opportunity Type',
        options: opportunitytypes,
        isHidelabel: true,
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
    ],
    [
      {
        name: 'opportunityName',
        type: 'input',
        labelValue: '*Opportunity Name',
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
      {
        name: 'opportunityDescription',
        type: 'input',
        labelValue: 'Opportunity Description',
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
      {
        name: 'totalContractValueInfy',
        type: 'input',
        labelValue: '**TCV for Infosys (MUSD)',
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
      {
        name: 'totalContractValueEquinox',
        type: 'input',
        labelValue: '**TCV for Equinox (MUSD)',
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
    ],
    [
      {
        name: 'annualSubscription',
        type: 'input',
        labelValue: '**Annual Subscription (MUSD)',
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
      {
        name: 'monthlyHostingValue',
        type: 'input',
        labelValue: '**Monthly Hosting/Sass(MUSD)',
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
      {
        name: 'yearlySupport',
        type: 'input',
        labelValue: '**AMS/Yearly Support(MUSD)',
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
      {
        name: 'services',
        type: 'input',
        labelValue: '**Services (MUSD)',
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
    ],
    [
      {
        name: 'contractDurationInMonths',
        type: 'input',
        labelValue: '**Contract Duration (In Months)',
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
      {
        name: 'remarks',
        type: 'input',
        labelValue: 'Remarks',
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
      {
        name: 'submissionDate',
        type: 'date',
        labelValue: 'Submission Date',
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
      {
        name: 'pricingModel',
        type: 'select',
        labelValue: 'Pricing Model',
        options: pricingmodels,
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
    ],
    [
      {
        name: 'proposalAnchor',
        type: 'input',
        labelValue: 'Proposal Anchor',
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
      {
        name: 'deliveryAnchor',
        type: 'input',
        labelValue: 'Delivery Anchor',
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
      {
        name: 'verticalSales',
        type: 'input',
        labelValue: 'Vertical Sales',
        columnType: {
          lg: '2',
          md: '6',
          sm: '12',
        },
      },
      {
        name: 'practiceSales',
        type: 'input',
        labelValue: 'Practice Sales',
        columnType: {
          lg: '2',
          md: '6',
          sm: '12',
        },
      },
      {
        name: 'competitor',
        type: 'input',
        labelValue: 'Competitor',
        columnType: {
          lg: '2',
          md: '6',
          sm: '12',
        },
      },
    ],
    [
      {
        name: 'licenseSale',
        type: 'checkbox',
        isHideLabel: true,
        labelValue: 'License-Sale',
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
      {
        name: 'partnerLedRfp',
        type: 'checkbox',
        isHideLabel: true,
        labelValue: 'Partner-Led RFP',
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
      {
        name: 'newAccountOpening',
        type: 'checkbox',
        isHideLabel: true,
        labelValue: 'New Account Openings(NAO)',
        columnType: {
          lg: '3',
          md: '6',
          sm: '12',
        },
      },
    ],
    {
      name: 'submitbutton',
      type: 'button',
      columnType: {
        lg: '12',
        md: '12',
        sm: '12',
      },
    },
  ]
}
