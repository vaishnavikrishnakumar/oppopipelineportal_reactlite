import React, {useState, useEffect} from 'react'
import {Container, Button, Col} from 'react-bootstrap'
import {
  CommonForm,
  BreadcrumbList,
  Toast,
  showToast,
} from 'src/views/components'
import {newentryForminput} from './formInput'
import {validationschema} from './Validation'
import {Link} from 'react-router-dom'
import {transformNewopprtunity, usefetchresponseAPI} from 'src/Utils'
import './style.scss'

function NewEntry(props) {
  const [customerNamevalue, setCustomerName] = useState('')
  const breadcrumblists = [
    {
      label: 'New Entry',
    },
  ]

  const {action, response} = usefetchresponseAPI({
    endpointName: 'addNewopp',
  })

  useEffect(() => {
    if (response.status == 200) {
      showToast({
        message: 'Opportunity Successfully Added',
        type: 'default',
      })
    } else if (response.status == 500) {
      showToast({
        message: 'Something went Wrong',
        type: 'default',
      })
    }
  }, [response])

  const handlesubmit = async data => {
    let {customerName} = data
    if (customerName == '') {
      data['customerName'] = customerNamevalue
    }
    if (data['customerName'] != '') {
      setCustomerName('')
    }
    await action({postdata: transformNewopprtunity(data)})
  }

  const renderButton = btnprops => {
    return (
      <Container className="mw-100 mt-2 d-flex justify-content-center">
        <Col lg={2} md={4} sm={12} className="mw-100">
          <Button
            variant={
              !(btnprops.dirty && btnprops.isValid) ? 'secondary' : 'primary'
            }
            type="submit"
            className="my-2 btn-block"
            disabled={!(btnprops.dirty && btnprops.isValid)}>
            Save
          </Button>
        </Col>
        <Col lg={2} md={4} sm={12}>
          <Link className="newentry-cancel" to="/">
            <Button block variant="secondary" className="my-2 mx-2 btn-block">
              Cancel
            </Button>
          </Link>
        </Col>
      </Container>
    )
  }

  const handleMCCselection = event => {
    const selectedValue = JSON.parse(
      event.target.options[event.target.selectedIndex].getAttribute('fullValue')
    )
    setCustomerName(selectedValue.clientName)
  }

  const formHandler = {
    handleMCCselection,
    customerNamevalue,
  }

  return (
    <Container className="newoppo-wrapper mw-100">
      <BreadcrumbList listData={breadcrumblists} />
      <Container className="mw-100">
        <span>
          Please Note : * Marked Field as Mandatory . **Marked field should be
          Number only
        </span>
        <p className="text-danger">
          if Master code doesn't exist in the list,Please raise a request to
          XXXX with the Master Customer code, Client
          Name,Region,Vertical,Segment
        </p>
      </Container>
      <CommonForm
        formInput={newentryForminput(formHandler)}
        onSubmit={handlesubmit}
        Buttoncomponent={renderButton}
        Schema={validationschema()}
        formClassName="mt-3"
      />
      <Toast
        options={{
          wrapperId: 'new-oppo-wrapper',
          autoClose: false,
          onCloseClick: () => {
            props.history.push('/')
          },
        }}
      />
    </Container>
  )
}

export {NewEntry}

export default NewEntry
