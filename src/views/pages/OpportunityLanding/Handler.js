import React, {useState, useContext, useEffect} from 'react'
import {Appcontext} from 'src/App/Appcontext'
import {useGetAllopportunities} from 'src/Endpoints/Opportunity'
import {Oppcontext} from './OpportunityLanding'
import {usefetchresponseAPI, useAdditionaldata} from 'src/Utils'
import {Validation} from './Validation'
const userEditablestatus = [
  'In Progress',
  'Submitted',
  'SOW Signed',
  'On Hold',
  'Selected For Orals',
]

const noneditableAttributes = [
  'ciaraId',
  'region',
  'segment',
  'vertical',
  'clientName',
  'creationDate',
]

function useHandler() {
  const {oppos, setOpprtunities} = useContext(Oppcontext)
  const [currentpage, setCurrentpage] = useState(0)
  const [Editlist, setEditList] = useState([])
  const [editedfield, setEditfield] = useState({})
  const [showerror, setShowerror] = useState(false)
  const [error, setError] = useState([])
  const {user} = useContext(Appcontext)
  const {
    opportunitytypes,
    partenrunits,
    pricingmodels,
    statuslist,
    masterCustomerCodes,
    winlossreasons,
  } = useAdditionaldata(true)

  const additionalData = {
    opportunitytypes,
    partenrunits,
    pricingmodels,
    statuslist,
    masterCustomerCodes,
    winlossreasons,
  }

  const queryparams = {
    pageNo: 0,
  }

  const opportunities = usefetchresponseAPI({
    endpointName: 'getAllopp',
    queryparams,
  })

  const saveopportunities = usefetchresponseAPI({
    endpointName: 'editopps',
  })

  useEffect(() => {
    if (opportunities.response.status == 200) {
      document.getElementById('oppotable').reset()
      setOpprtunities(opportunities.response.opportunities)
    }
  }, [opportunities.response])

  const isEnableedit = opp => {
    const isAdmin = user.total_pages == 2
    if (!isAdmin) {
      const isvalidRow = !userEditablestatus.includes(opp['opportunityStatus'])
      return opp['opportunityStatus'] === undefined ? false : !isvalidRow
    } else {
      return false
    }
  }

  const AddEditlist = opp => {
    const index = Editlist.findIndex(element => element.ciaraId === opp.ciaraId)
    if (index < 0) {
      setEditList(prevArr => {
        return [...prevArr, opp]
      })
    } else {
      const oppindex = oppos.findIndex(
        element => element.ciaraId === opp.ciaraId
      )
      const updatedList = Editlist
      updatedList.splice(index, 1)
      Revertchanges(oppindex)
      setEditList([...updatedList])
    }
  }

  const Revertchanges = oppindex => {
    const removeID = oppos[oppindex]['ciaraId']
    for (let oldvalue in editedfield[removeID]) {
      programaticallyFormupdate(oldvalue, oppos[oppindex][oldvalue], oppindex)
    }
  }

  const programaticallyFormupdate = (field, value, index) => {
    let eventinput = new Event('change')
    const element = document.getElementsByClassName(field)[index]
    if (element !== undefined) {
      element.value = value
      element.dispatchEvent(eventinput)
    }
  }

  const isDisable = (field, id) => {
    const isavailID = Editlist.findIndex(element => {
      return element.ciaraId == id
    })
    const isvalidfield = noneditableAttributes.includes(field)
    return isavailID >= 0 ? isvalidfield : true
  }

  const handleOnchange = (event, value, id) => {
    const index = Editlist.findIndex(element => element.ciaraId === id)
    const ModifiedLists = Editlist
    let Editedopp = ModifiedLists[index]
    const isvalid = Validation(
      value,
      event.target.value,
      Editedopp['opportunityStatus'],
      additionalData
    )
    if (!isvalid) {
      if (!error.includes(value)) {
        setError(preverr => [...preverr, value])
      }
    } else if (isvalid) {
      if (error.includes(value)) {
        let errIndex = error.findIndex(e => e === value)
        let backupArray = error
        backupArray.splice(errIndex, 1)
        setError(backupArray)
      }
    }
    let updatedfields = {
      ...editedfield[id],
      [value]: event.target.value,
    }

    if (value === 'masterCustomerCode') {
      updatedfields = {
        ...editedfield[id],
        [value]: event.target.value,
        ...handleMCCselection(event, Editedopp),
      }
    }
    setEditfield({...editedfield, [id]: updatedfields})
  }

  const handleMCCselection = (event, updatedopp) => {
    const selectedindex = oppos.findIndex(
      element => element.ciaraId === updatedopp.ciaraId
    )
    const selectedValue = JSON.parse(
      event.target.options[event.target.selectedIndex].getAttribute('fullValue')
    )
    const mccrelatedfields = {
      vertical: selectedValue.vertical,
      segment: selectedValue.segment,
      region: selectedValue.region,
      clientName: selectedValue.clientName,
    }
    programaticallyFormupdate('vertical', selectedValue.vertical, selectedindex)
    programaticallyFormupdate('segment', selectedValue.segment, selectedindex)
    programaticallyFormupdate('region', selectedValue.region, selectedindex)
    programaticallyFormupdate(
      'clientName',
      selectedValue.clientName,
      selectedindex
    )
    return mccrelatedfields
  }

  const updateCall = async () => {
    if (error.length <= 0 && Editlist.length >= 1) {
      setShowerror(false)
      const updatedList = Editlist.map(listitem => {
        return {...listitem, ...editedfield[listitem.ciaraId]}
      })
      await saveopportunities.action({postdata: updatedList})
      setError([])
      await opportunities.action()
      setEditList([])
    } else {
      setShowerror(true)
    }
  }

  const cancelChanges = async () => {
    if (Editlist.length > 0) {
      document.getElementById('oppotable').reset()
      setEditList([])
    }
  }

  const actions = {
    AddEditlist,
    isDisable,
    handleOnchange,
    isEnableedit,
    updateCall,
    cancelChanges,
  }

  const setPage = async count => {
    const queryparams = {
      pageNo: count - 1,
    }
    await opportunities.action({queryparams})
    document.getElementById('oppotable').reset()
  }

  const paginationhandler = {setPage}

  return {
    actions,
    Editlist,
    error,
    showerror,
    paginationhandler,
  }
}

export {useHandler}

export default useHandler
