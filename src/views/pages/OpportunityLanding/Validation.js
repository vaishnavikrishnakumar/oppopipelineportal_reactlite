import {getMastercode, useAdditionaldata} from 'src/Utils'

const checkEnumvalue = (value, Enumvalues) => {
  let validationArray = Array(Enumvalues.length)
    .fill(0)
    .map((el, index) => Enumvalues[index]['value'])
  return validationArray.includes(value)
}

const checkBoolean = (value, isBoolean) => {
  if (isBoolean) {
    return value === true || value === false ? true : false
  } else {
    const values = ['yes', 'no', 'true', 'false']
    return values.includes(value.toLowerCase())
  }
}

const checkEmpty = value => {
  return value !== '' ? true : false
}

const checkNumberfield = value => {
  const number = Number(value)
  return !isNaN(number)
}

const numberlimitcheck = (field, value) => {
  let paresedvalue = parseInt(value)
  if (field === 'monthlyHostingValue' || field === 'contractDurationInMonths') {
    return paresedvalue >= 0 && paresedvalue <= 100
  }
  return paresedvalue >= 0 && paresedvalue <= 1000
}

const Charactercheck = value => {
  return value.match(/^[A-Za-z]+$/) === null ? false : true
}

export function Validation(field, value, status, additionalData) {
  if (field === 'opportunityType') {
    return checkEmpty(value)
  }

  if (field === 'masterCustomerCode') {
    return (
      checkEmpty(value) &&
      checkEnumvalue(value, additionalData.masterCustomerCodes)
    )
  }

  if (field === 'partnerUnit') {
    return (
      checkEmpty(value) && checkEnumvalue(value, additionalData.partenrunits)
    )
  }

  if (field === 'opportunityType') {
    return (
      checkEmpty(value) &&
      checkEnumvalue(value, additionalData.opportunitytypes)
    )
  }

  if (field === 'totalContractValueInfy') {
    return (
      checkNumberfield(value) &&
      numberlimitcheck('totalContractValueInfy', value)
    )
  }

  if (field === 'monthlyHostingValue') {
    return (
      checkNumberfield(value) && numberlimitcheck('monthlyHostingValue', value)
    )
  }

  if (field === 'totalContractValueEquinox') {
    return (
      checkNumberfield(value) &&
      numberlimitcheck('totalContractValueEquinox', value)
    )
  }

  if (field === 'yearlySupport') {
    return checkNumberfield(value) && numberlimitcheck('yearlySupport', value)
  }

  if (field === 'annualSubscription') {
    return (
      checkNumberfield(value) && numberlimitcheck('annualSubscription', value)
    )
  }

  if (field === 'contractDurationInMonths') {
    return (
      checkNumberfield(value) &&
      numberlimitcheck('contractDurationInMonths', value)
    )
  }

  if (field === 'opportunityStatus') {
    return checkEmpty(value) && checkEnumvalue(value, additionalData.statuslist)
  }

  if (field === 'pricingModel' && status === 'Submitted') {
    return (
      checkEmpty(value) && checkEnumvalue(value, additionalData.pricingmodels)
    )
  }

  if (field === 'newAccountOpening') {
    return checkEmpty(value) && checkBoolean(value, false)
  }

  if (field === 'licenseSale') {
    return checkEmpty(value) && checkBoolean(value, false)
  }

  if (field === 'proposalAnchor') {
    return checkEmpty(value) && Charactercheck(value)
  }

  if (field === 'deliveryAnchor') {
    let deliveryAnchorchk = true
    if (status === 'Submitted') {
      deliveryAnchorchk = Charactercheck(value)
    }
    return checkEmpty(value) && deliveryAnchorchk
  }

  if (field === 'verticalSales') {
    let verticalSaleschk = true
    if (status === 'Submitted') {
      verticalSaleschk = Charactercheck(value)
    }
    return checkEmpty(value) && verticalSaleschk
  }

  if (field === 'competitor') {
    let competitorchk = true
    if (status === 'Won' || status === 'Lost') {
      competitorchk = Charactercheck(value)
    }
    return checkEmpty(value) && competitorchk
  }

  if (field === 'winLossReason') {
    let competitorchk = true
    if (status === 'Won' || status === 'Lost') {
      competitorchk = Charactercheck(value)
    }
    return checkEmpty(value) && competitorchk
  }
  return true
}
