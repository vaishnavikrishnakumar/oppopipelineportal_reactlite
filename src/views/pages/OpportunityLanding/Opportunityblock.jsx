import React, {useContext, useState} from 'react'
import {Container, Row, Col, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import {FormTable, Pagination} from 'src/views/components'
import {getLandingData} from './fixture'
import {useHandler} from './Handler'
import {Oppcontext} from './OpportunityLanding'
import {useAdditionaldata} from 'src/Utils'
import {useGetAllopportunities} from 'src/Endpoints/Opportunity'
import './style.scss'

function Opportunityblock() {
  const {oppos} = useContext(Oppcontext)
  const {actions, error, showerror, paginationhandler} = useHandler()
  const {
    opportunitytypes,
    partenrunits,
    pricingmodels,
    statuslist,
    masterCustomerCodes,
    winlossreasons,
  } = useAdditionaldata(true)
  const additionalData = {
    opportunitytypes,
    partenrunits,
    pricingmodels,
    statuslist,
    masterCustomerCodes,
    winlossreasons,
  }
  return (
    <Container className="mw-100 mt-3 oppoBlock">
      {Array.isArray(oppos) && oppos.length > 0 && (
        <>
          {showerror && error.join('')}
          <Row className="mw-100 opp-actions px-0 pb-2">
            <Col
              lg={6}
              md={6}
              sm={12}
              data-testid="qa-oppolanding-totalwin"
              className="wondetails d-flex justify-content-start align-items-center text-primary">
              <div>Total Won</div>
            </Col>
            <Col
              lg={6}
              md={6}
              sm={12}
              className="p-0 actiondetails d-flex justify-content-end align-items-center">
              <Link data-testid="qa-oppolanding-help" to="/help">
                Help
              </Link>
              <Button className="mx-2" onClick={() => actions.updateCall()}>
                Save Changes
              </Button>
              <Button
                variant="secondary"
                onClick={() => actions.cancelChanges()}>
                Cancel Changes
              </Button>
            </Col>
          </Row>
          <FormTable
            titles={getLandingData(additionalData)}
            displaycontent={oppos}
            isFormtable={true}
            id={'oppotable'}
            handler={actions}
          />
          <Pagination pageHandler={paginationhandler.setPage} />
        </>
      )}
    </Container>
  )
}

export {Opportunityblock}

export default Opportunityblock
