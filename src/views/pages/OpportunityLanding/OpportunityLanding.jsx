import React, {createContext, useState, useEffect} from 'react'
import {Container} from 'react-bootstrap'
import {Filter} from 'src/views/components'
import {Opportunityblock} from './Opportunityblock'
import {useGetAllopportunities} from 'src/Endpoints/Opportunity'
import {sampleLandingData} from './fixture'

const Oppcontext = createContext()

function OpportunityLanding(props) {
  const [opprtunities, setOpprtunities] = useState([])
  const opprtunitiesresponse = useGetAllopportunities({
    pageNo: 0,
  })
  useEffect(() => {
    setOpprtunities(opprtunitiesresponse.opportunities)
  }, [opprtunitiesresponse])
  return (
    <Oppcontext.Provider
      value={{
        oppos: opprtunities,
        setOpprtunities,
        totalcount: opprtunitiesresponse.totalOpportunitiesCount,
      }}>
      <Container className="oppo-wrapper mw-100">
        <Container className="d-none d-lg-block filterform  mw-100 pt-2 d-md-flex d-lg-flex">
          <Filter />
        </Container>
        <Opportunityblock />
      </Container>
    </Oppcontext.Provider>
  )
}

export {OpportunityLanding, Oppcontext}

export default OpportunityLanding
