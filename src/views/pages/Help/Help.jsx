import React from 'react'
import Table from 'react-bootstrap/Table'
import Data from '../Help/mock-data.json'
import {Container} from 'react-bootstrap'
function Help() {
  const details = Data
  return (
    <div>
      <Container className="oppo-wrapper mw-100">
        <h5 data-testid="qa-help-title" className="text-center">
          FIELD DETAILS
        </h5>
        <Table bordered striped hover responsive size="md">
          <thead className="text-light bg-dark">
            <tr>
              <th data-testid="qa-helptitle-fields">FIELDS</th>
              <th data-testid="qa-helptitle-description">DESCRIPTION</th>
            </tr>
          </thead>
          <tbody>
            {details.map((data, index) => (
              <tr>
                <td data-testid={`qa-helpfield`}>{data.field}</td>
                <td data-testid={`qa-helpfielddesc`}>{data.description}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    </div>
  )
}
export {Help}
export default Help
