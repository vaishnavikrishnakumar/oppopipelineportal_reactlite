const manualentryInput = [
  {
    name: 'mastercustomercode',
    type: 'input',
    labelValue: '*Master Customer Code',
    columnType: {
      lg: '6',
      md: '6',
      sm: '12',
    },
  },
  {
    name: 'clientname',
    type: 'input',
    labelValue: '*Client Name',
    columnType: {
      lg: '6',
      md: '6',
      sm: '12',
    },
  },
  {
    name: 'region',
    type: 'input',
    labelValue: '*Region',
    columnType: {
      lg: '6',
      md: '6',
      sm: '12',
    },
  },
  {
    name: 'vertical',
    type: 'input',
    labelValue: '*Vertical',
    columnType: {
      lg: '6',
      md: '6',
      sm: '12',
    },
  },
  {
    name: 'segment',
    type: 'input',
    labelValue: '*Segment',
    columnType: {
      lg: '6',
      md: '6',
      sm: '12',
    },
  },
  {
    name: 'submitbutton',
    type: 'button',
    columnType: {
      lg: '12',
      md: '12',
      sm: '12',
    },
  },
]

export {manualentryInput}
