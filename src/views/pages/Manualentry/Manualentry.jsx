import React, {useState, useEffect, useContext} from 'react'
import {Container, Col, Button} from 'react-bootstrap'
import {
  BreadcrumbList,
  CommonForm,
  Toast,
  showToast,
} from 'src/views/components'
import {Link} from 'react-router-dom'
import {manualentryInput} from './formInput'
import {validationschema} from './Validation'
import {usefetchresponseAPI} from 'src/Utils'
import {useGetAllmastercode} from 'src/Endpoints/Opportunity'
import {Appcontext} from 'src/App/Appcontext'

function Manualentry(props) {
  const {opportunities} = useContext(Appcontext)
  const {action, response} = usefetchresponseAPI({
    endpointName: 'addMastercode',
  })

  const getallmcc = usefetchresponseAPI({
    endpointName: 'getMastercode',
  })

  const getModifiedData = data => {
    const {region, vertical, segment, clientname, mastercustomercode} = data
    return {
      masterCustomerCode: mastercustomercode,
      clientName: clientname,
      region,
      vertical,
      segment,
    }
  }

  useEffect(() => {
    if (response.status == 200) {
      showToast({message: 'Master Code Successfully Added', type: 'default'})
    } else if (response.status == 500) {
      showToast({
        message: 'Something went Wrong',
        type: 'default',
      })
    }
  }, [response])

  useEffect(() => {
    opportunities.setMcc(getallmcc.response)
  }, [getallmcc.response])

  const handlesubmit = async data => {
    await action({postdata: getModifiedData(data)})
    await getallmcc.action()
  }

  const list = [
    {
      label: 'Manual Entry',
    },
  ]
  const renderButton = btnprops => {
    return (
      <Container className="mw-100 mt-2 d-flex justify-content-center">
        <Col lg={2} md={4} sm={12} className="mw-100">
          <Button
            variant={
              !(btnprops.dirty && btnprops.isValid) ? 'secondary' : 'primary'
            }
            block
            type="submit"
            className="my-2 btn-block"
            disabled={!(btnprops.dirty && btnprops.isValid)}>
            Save
          </Button>
        </Col>
        <Col lg={2} md={4} sm={12}>
          <Link className="newentry-cancel" to="/">
            <Button block variant="secondary" className="my-2 mx-2 btn-block">
              Cancel
            </Button>
          </Link>
        </Col>
      </Container>
    )
  }
  return (
    <Container className="Manualentry-wrapper mw-100 p-0">
      <BreadcrumbList listData={list} />
      <Container className="mw-100 py-2">
        <span data-testid={`qa-manualentry-note`}>
          Please Note : * Marked Field as Mandatory . **Marked field should be
          Number only
        </span>
        <p className="text-danger mb-0" data-testid={`qa-manualentry-suggest`}>
          if Master code doesn't exist in the list,Please raise a request to
          XXXX with the Master Customer code, Client
          Name,Region,Vertical,Segment
        </p>
      </Container>
      <Container className="mw-100 pt-0">
        <CommonForm
          formInput={manualentryInput}
          onSubmit={handlesubmit}
          Buttoncomponent={renderButton}
          Schema={validationschema()}
          formClassName="mt-3"
        />
      </Container>
      <Toast
        options={{wrapperId: 'manual-cc-wrapper', onCloseClick: () => {}}}
      />
    </Container>
  )
}

export {Manualentry}

export default Manualentry
