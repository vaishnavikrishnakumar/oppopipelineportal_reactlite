import * as yup from 'yup'

const validationschema = () => {
  const schema = {
    mastercustomercode: yup
      .string()
      .required('Please Enter Master Customer Code'),
    clientname: yup.string().required('Please Enter Client Name'),
    region: yup.string().required('Please Enter Region Name'),
    vertical: yup.string().required('Please Enter Vertical Name'),
    segment: yup.string().required('Please Enter Segment Name'),
  }
  return yup.object(schema)
}

export {validationschema}
