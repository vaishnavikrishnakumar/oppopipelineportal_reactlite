export const Base_path = `https://reqres.in/api`
export const opp_path = `http://localhost:8086`

export const URL = {
  base: Base_path,
  user: `${Base_path}/users`,
  getmaster: `${opp_path}/getAllMasterCustomerCode`,
  addmaster: `${opp_path}/addNewMasterCustomerCode`,
  addopp: `${opp_path}/addNewOpportunity`,
  getopp: `${opp_path}/getAllOpportunity`,
  gerallPartner: `${opp_path}/getAllPatrners`,
  getallpricingmodel: `${opp_path}/getAllPricingModels`,
  getallopportunity: `${opp_path}/getAllOpportunityTypes`,
  getallstatus: `${opp_path}/getAllOpportunityStatuses`,
  getallwinlossreason: `${opp_path}/getWinLossReasonList`,
  editopps: `${opp_path}/editOpportunities`,
  exportopp: `${opp_path}/getExportedOpportunities`,
  // opportunityUpload: `${opp_path}/`
}

export const URL_DETAILS = {
  getusers: {
    url: URL.user,
    method: 'GET',
  },
  getMastercode: {
    url: URL.getmaster,
    method: 'GET',
  },
  addNewopp: {
    url: URL.addopp,
    method: 'POST',
  },
  addMastercode: {
    url: URL.addmaster,
    method: 'POST',
  },
  getAllopp: {
    url: URL.getopp,
    method: 'GET',
  },
  gerallPartner: {
    url: URL.gerallPartner,
    method: 'GET',
  },
  getallpricingmodel: {
    url: URL.getallpricingmodel,
    method: 'GET',
  },
  getallopportunitytype: {
    url: URL.getallopportunity,
    method: 'GET',
  },
  getallwinlossreason: {
    url: URL.getallwinlossreason,
    method: 'GET',
  },
  getallstatus: {
    url: URL.getallstatus,
    method: 'GET',
  },
  editopps: {
    url: URL.editopps,
    method: 'PUT',
  },
  exportopp: {
    url: URL.exportopp,
    method: 'GET',
  },
}
