import React, {useMemo} from 'react'
import {usefetchresponseAPI} from 'src/Utils'

function getUserdetails() {
  const {action, response} = usefetchresponseAPI({
    endpointName: 'getusers',
  })
  useMemo(async () => await action(), [])
  return response
}

export {getUserdetails}
export default getUserdetails
