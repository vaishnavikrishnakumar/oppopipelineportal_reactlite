import React, {useMemo} from 'react'
import {usefetchresponseAPI} from 'src/Utils'

function useGetAllmastercode() {
  const {action, response} = usefetchresponseAPI({
    endpointName: 'getMastercode',
  })
  useMemo(async () => await action(), [])
  return response
}

function useGetAllopportunities(props) {
  const {pageNo = 1} = props
  const queryparams = {
    pageNo,
  }
  const {action, response} = usefetchresponseAPI({
    endpointName: 'getAllopp',
    queryparams,
  })

  useMemo(async () => await action(), [])
  return response
}

function useGetAllPartner() {
  const {action, response} = usefetchresponseAPI({
    endpointName: 'gerallPartner',
  })
  useMemo(async () => await action(), [])
  return response
}

function useGetAllPricingmodel() {
  const {action, response} = usefetchresponseAPI({
    endpointName: 'getallpricingmodel',
  })
  useMemo(async () => await action(), [])
  return response
}

function useGetAllOpportunitytype() {
  const {action, response} = usefetchresponseAPI({
    endpointName: 'getallopportunitytype',
  })
  useMemo(async () => await action(), [])
  return response
}

function useGetAllstatus() {
  const {action, response} = usefetchresponseAPI({
    endpointName: 'getallstatus',
  })
  useMemo(async () => await action(), [])
  return response
}

function useGetAllwinlossreason() {
  const {action, response} = usefetchresponseAPI({
    endpointName: 'getallwinlossreason',
  })
  useMemo(async () => await action(), [])
  return response
}

export {
  useGetAllmastercode,
  useGetAllopportunities,
  useGetAllPartner,
  useGetAllPricingmodel,
  useGetAllOpportunitytype,
  useGetAllstatus,
  useGetAllwinlossreason,
}
export default useGetAllmastercode
