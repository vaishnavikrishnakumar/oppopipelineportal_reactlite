import querystring from 'querystring'
import { API_CONFIG, END_POINTS, enableProxy } from 'src/bootstrapper/env'

async function fetchResponseFromAPI(apiConfig) {
  const {
    url,
    headers = {},
    method = 'GET',
    containerName,
    pathParams,
    queryParams,
    isToStoreInLocal,
    storageKey,
    endPointName,
  } = apiConfig

  const apiUrl = url ? url : generateAPIURL(endPointName)
  const combinedHeaders = Object.assign(headers, API_CONFIG.commonHeaders)

  let queryPrefix = ''

  if (pathParams && enableProxy) {
    queryPrefix = '&'
  } else if (queryParams) {
    queryPrefix = '?'
  }
  const queryParamString = queryPrefix + querystring.stringify(queryParams)

  let finalUrl =
    apiUrl +
    (pathParams && enableProxy
      ? `?pathParams=${pathParams}`
      : `/${pathParams}`) +
    queryParamString

  // if (!enableProxy) {
  //   finalUrl = finalUrl.replace(API_CONFIG.proxyDomain, API_CONFIG.apiDomain)
  // }

  if (isToStoreInLocal) {
    const localResponse = getFromStorage({ url: finalUrl })
    if (localResponse) {
      return localResponse
    }
  }

  const ModifiedHeader = new Headers(combinedHeaders)

  return await fetch(finalUrl, {
    method,
    mode: 'cors',
    headers: ModifiedHeader,
  })
    .then(function(response) {
      return response.json()
    })
    .then(function(jsonResponse) {
      if (isToStoreInLocal) {
        setInStorage({
          url: storageKey ? storageKey : finalUrl,
          response: jsonResponse,
        })
      }
      return jsonResponse
    })
}

function generateAPIURL(endPointName) {
  return END_POINTS[endPointName].url
}

function setInStorage(props) {
  const { url, response } = props
  setLocalStorage(url, response)
}

function getFromStorage({ url }) {
  const localData = getLocalStorage(url)
  if (localData) {
    return localData
  } else {
    return undefined
  }
}

function setLocalStorage(storageName, storageItem) {
  try {
    if (typeof window !== 'undefined') {
      localStorage.setItem(storageName, JSON.stringify(storageItem))
    }
  } catch (e) {
    console.error('Exception in storage', e)
  }
}

function getLocalStorage(storageName) {
  try {
    if (typeof window !== 'undefined') {
      return JSON.parse(localStorage.getItem(storageName))
    }
  } catch (e) {
    console.error('Exception in storage', e)
  }
}

export default fetchResponseFromAPI
export { fetchResponseFromAPI }
