const pageNames = {
  home: '/',
  newentry: '/newentry',
  help: '/help',
  import: '/import',
  manualentry: '/manualentry',
  bulkimport: '/bulkimport',
}

const pathParams = [
  {
    path: pageNames.newentry,
    pageName: 'NewEntry',
  },
  {
    path: pageNames.help,
    pageName: 'Help',
  },
  {
    path: pageNames.import,
    pageName: 'Import',
  },
  {
    path: pageNames.bulkimport,
    pageName: 'BulkImport',
  },
  {
    path: pageNames.manualentry,
    pageName: 'Manualentry',
  },
  {
    path: pageNames.home,
    pageName: 'OpportunityLanding',
  },
]

export default pathParams
export {pathParams, pageNames}
