import React from 'react'
import {Switch, Route} from 'react-router'
import loadable from '@loadable/component'
import pathParams from 'src/routes/pathParams'
import {AppLoader} from 'src/views/components'
import {Header} from 'src/views/components/Header'
import {NewEntry} from 'src/views/pages/NewEntry'
import {OpportunityLanding} from 'src/views/pages/OpportunityLanding'
import {Import} from 'src/views/pages/Import'
import {BulkImport} from 'src/views/pages/BulkImport'
import {Manualentry} from 'src/views/pages/Manualentry'

// @Refer https://webpack.js.org/api/module-methods/
const LoadableComponent = loadable(props => {
  return import(`src/views/pages/${props.pageName}`)
})

const renderRoutes = routes => {
  return routes.map((route, index) => {
    const {path} = route
    return (
      <Route
        key={route.key || index}
        path={path}
        exact
        strict
        render={routeProps => {
          return <LoadableComponent {...routeProps} {...route} />
        }}
      />
    )
  })
}

const getRoutes = () => {
  return pathParams.map(routeConfig)
}

class Routes extends React.Component {
  render() {
    return (
      <React.Fragment>
        <AppLoader isLoading={true} />
        <Header />
        <section className="app-wrapper">
          <Switch>{renderRoutes(pathParams)}</Switch>
        </section>
      </React.Fragment>
    )
  }
}
export default Routes
export {Routes}
