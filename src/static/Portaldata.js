const opportunitytypeData = [
  {
    label: 'Extension',
    value: 'Extension',
  },
  {
    label: 'Pursuit',
    value: 'Pursuit',
  },
  {
    label: 'RFI',
    value: 'RFI',
  },
  {
    label: 'Proactive',
    value: 'Proactive',
  },
]

const partnerunitData = [
  {
    label: 'DX',
    value: 'DX',
  },
  {
    label: 'ADM',
    value: 'ADM',
  },
  {
    label: 'Equinox',
    value: 'Equinox',
  },
  {
    label: 'Others',
    value: 'Others',
  },
]

const statusData = [
  {
    label: 'In Progress',
    value: 'In Progress',
  },
  {
    label: 'Submitted',
    value: 'Submitted',
  },
  {
    label: 'Won',
    value: 'Won',
  },
  {
    label: 'SOW Signed',
    value: 'SOW Signed',
  },
  {
    label: 'Lost',
    value: 'Lost',
  },
  {
    label: 'On Hold',
    value: 'On Hold',
  },
  {
    label: 'Abandoned',
    value: 'Abandoned',
  },
  {
    label: 'Disqualified for Equinox',
    value: 'Disqualified for Equinox',
  },
  {
    label: 'Selected for Orals',
    value: 'Selected for Orals',
  },
]

const pricingmodelData = [
  {
    label: 'FP',
    value: 'FP',
  },
  {
    label: 'T&M',
    value: 'T&M',
  },
  {
    label: 'Outcome Based',
    value: 'Outcome Based',
  },
  {
    label: 'Not Applicable',
    value: 'Not Applicable',
  },
]

const naoData = [
  {
    label: 'Yes',
    value: true,
  },
  {
    label: 'No',
    value: false,
  },
]

function getLandingData() {
  return [
    {
      label: 'ID',
      value: 'id',
      type: 'input',
    },
    {
      label: 'Opportunity Type',
      value: 'opportunitytype',
      type: 'select',
      options: opportunitytypeData,
    },
    {
      label: 'Partner Unit',
      value: 'partnerunit',
      type: 'select',
      options: partnerunitData,
    },
    {label: 'Region', value: 'region', type: 'input'},
    {label: 'Segment', value: 'segment', type: 'input'},
    {label: 'Vertical', value: 'vertical', type: 'input'},
    {
      label: 'Master Customer Code',
      value: 'mastercustomercode',
      type: 'select',
    },
    {label: 'Customer Name', value: 'customername', type: 'input'},
    {label: 'Opportunity Name', value: 'opportunityname', type: 'input'},
    {
      label: 'Opportunity Description',
      value: 'opportunitydesc',
      type: 'input',
    },
    {label: 'Remarks', value: 'remarks', type: 'input'},
    {
      label: 'Status',
      value: 'status',
      type: 'select',
      options: statusData,
    },
    {label: 'TCV for Infosys', value: 'tcvforinfosys', type: 'input'},
    {label: 'TCV for Equinox', value: 'tcvforequinox', type: 'input'},
    {
      label: 'Contract Duration',
      value: 'contractduration',
      type: 'input',
    },
    {
      label: 'Pricing Model',
      value: 'pricingmodel',
      type: 'select',
      options: pricingmodelData,
    },
    {label: 'Probability', value: 'probability', type: 'input'},
    {label: 'Creation Date', value: 'creationdate', type: 'date'},
    {label: 'Submission Date', value: 'submissiondate', type: 'date'},
    {label: 'Closure Date', value: 'closuredate', type: 'date'},
    {label: 'Start Date', value: 'startdate', type: 'date'},
    {label: 'End Date', value: 'enddate', type: 'date'},
    {label: 'NAO', value: 'nao', type: 'select', options: naoData},
    {label: 'Proposal Anchor', value: 'proposalanchor', type: 'input'},
    {label: 'Delivery Anchor', value: 'deliveryanchor', type: 'input'},
    {label: 'Architect Anchor', value: 'architectanchor', type: 'input'},
    {label: 'Practice Sales', value: 'practicesales', type: 'input'},
    {label: 'Vertical Sales', value: 'verticalsales', type: 'input'},
    {label: 'License Sale', value: 'licensesale', type: 'input'},
    {
      label: 'Annual Subscription',
      value: 'annualsubscription',
      type: 'input',
    },
    {label: 'Monthly Hosting', value: 'monthlyhosting', type: 'input'},
    {
      label: 'AMS /Yearly Support',
      value: 'amsyearlysupport',
      type: 'input',
    },
    {label: 'Competitor', value: 'competitor', type: 'input'},
    {label: 'Win Loss Reason', value: 'winlossreason', type: 'select'},
    {
      label: 'Win Loss Reason Details',
      value: 'winlossreasondetails',
      type: 'input',
    },
  ]
}

export {
  getLandingData,
  opportunitytypeData,
  partnerunitData,
  statusData,
  pricingmodelData,
  naoData,
}
