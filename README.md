# react-lite

List of todo's
1. Webpack dynamic imports - done
2. Webpack chunk names - done
3. Path params and dynamic routing - done
4. Use mobx - done
5. Eslint - done
6. Prettier - done
7. Making common API's like view bag
8. React helmet to use title's
9. Loadash
10. Context for all API's
11. Immutable
12. Css loader extractor - done
13. Image loader
14. Refernce links from where we get all those info's
15. Output files as JS instead of storing in memory (step for PDN)
16. Production ready codes
17. Storing API's in localstorage as common - done but not needed as of now
18. Jest testing
19. Loggers
20. pm2 if needed
21. Dynamic store id and domains
22. version control and status page
23. Skava - Boilerplate