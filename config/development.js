import webpack from 'webpack'
import WebpackDevServer from 'webpack-dev-server'
import configDevClient from './webpack.dev-client.js'
import { APP_PORT } from '../src/bootstrapper/env'

const isProd = process.env.NODE_ENV === 'production'
const PORT = process.env.PORT || APP_PORT || 5000

const showSuccessAfterInit = () => {
  console.log(
    `Server listening on \x1b[42m\x1b[1mhttp://localhost:${PORT}\x1b[0m in \x1b[41m${
      process.env.NODE_ENV
    }\x1b[0m 🌎...`
  )
}

if (isProd) {
  // @todo for production
} else {
  // const compiler = webpack([configDevClient, configDevServer])
  const compiler = webpack(configDevClient)

  var webPackServer = new WebpackDevServer(compiler, {
    historyApiFallback: true,
  })

  webPackServer.listen(PORT, 'localhost', () => {
    showSuccessAfterInit()
  })
}
