const path = require('path')
const glob = require('glob')
const HtmlWebPackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const PurgecssPlugin = require('purgecss-webpack-plugin')

const PATHS = {
  src: path.join(__dirname, '../src'),
}

// const purgecssPlugin = new PurgecssPlugin({
//   paths: glob.sync(`${PATHS.src}/**/*`, {nodir: true}),
// })

const htmlWebpackPlugin = new HtmlWebPackPlugin({
  template: './src/App/index.html',
  filename: './index.html',
})

const cssWebpackPlugin = new MiniCssExtractPlugin({
  filename: '[name].css',
  chunkFilename: '[name].css',
})

module.exports = {
  entry: './src/App/App.js',
  mode: 'development',
  output: {
    filename: '[name]-bundle.[hash].js',
    chunkFilename: '[name].[hash].js',
    path: path.resolve(__dirname, '/dist'),
    publicPath: '/',
  },
  // optimization: {
  //   splitChunks: {
  //     chunks: 'all',
  //   },
  // },
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      src: path.resolve(__dirname, '../src/'),
    },
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.(js|jsx|ts|tsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.(s*)css$/,
        exclude: /node_modules/,
        use: [
          // {
          //   loader: "style-loader"
          // },
          {
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: 'css-loader',
            options: {
              modules: false,
              importLoaders: 2,
              localIdentName: '[name]_[local]_[hash:base64]',
              // sourceMap: true,
              minimize: true,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              ident: 'postcss',
            },
          },
          {
            loader: 'sass-loader',
            options: {
              implementation: require('sass'),
            },
          },
        ],
      },
    ],
  },
  plugins: [htmlWebpackPlugin, cssWebpackPlugin],
}
